﻿using Newtonsoft.Json;

namespace TribesAscendAPI.Data
{
    /// <summary>
    /// I belive these will all be arrays will need to fix
    /// </summary>
    public class MatchStats
    {       
        [JsonProperty("Class")]
        public string Class { get; set; }

        [JsonProperty("Damage")]
        public int Damage { get; set; }

        [JsonProperty("Kills")]
        public int Kills { get; set; }

        [JsonProperty("Map_Name")]
        public string MapName { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("TimePlayed")]
        public int TimePlayed { get; set; }

        [JsonProperty("Vehicles_Destroyed")]
        public int VehiclesDestroyed { get; set; }

        [JsonProperty("ret_msg")]
        public object RetMsg { get; set; }
    }
}
