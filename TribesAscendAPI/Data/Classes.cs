﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TribesAscendAPI.Data
{
    public enum Classes 
    {
        None,
        Brute,
        Doombringer,
        Infiltrator,
        Juggernaught,
        [Description("Naked Pathfinder")]
        Nakedpathfinder,
        Pathfinder,
        Raider,
        Sentinel,
        Soldier,
        Technician
    };
}
