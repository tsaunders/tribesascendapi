﻿using Newtonsoft.Json;

namespace TribesAscendAPI.Data
{
    public class Player
    {
        [JsonProperty("Assists")]
        public int Assists { get; set; }

        [JsonProperty("Base_Assets_Destroyed")]
        public int BaseAssetsDestroyed { get; set; }

        [JsonProperty("Base_Repairs")]
        public int BaseRepairs { get; set; }

        [JsonProperty("Base_Upgrades_Purchased")]
        public int BaseUpgradesPurchased { get; set; }

        [JsonProperty("Belt_Kills")]
        public int BeltKills { get; set; }

        [JsonProperty("Callin_Kills")]
        public int CallinKills { get; set; }

        [JsonProperty("Callins_Made")]
        public int CallinsMade { get; set; }

        [JsonProperty("Created_Datetime")]
        public string CreatedDatetime { get; set; }

        [JsonProperty("Deaths")]
        public int Deaths { get; set; }

        [JsonProperty("Flag_Caps")]
        public int FlagCaps { get; set; }

        [JsonProperty("Flag_Returns")]
        public int FlagReturns { get; set; }

        [JsonProperty("Full_Regenerations")]
        public int FullRegenerations { get; set; }

        [JsonProperty("Generators_Destroyed")]
        public int GeneratorsDestroyed { get; set; }

        [JsonProperty("Headshots")]
        public int Headshots { get; set; }

        [JsonProperty("High_Speed_Flag_Grabs")]
        public int HighSpeedFlagGrabs { get; set; }

        [JsonProperty("Kills")]
        public int Kills { get; set; }

        [JsonProperty("Kills_In_Vehicle")]
        public int KillsInVehicle { get; set; }

        [JsonProperty("Last_Login_Datetime")]
        public string LastLoginDatetime { get; set; }

        [JsonProperty("Level")]
        public int Level { get; set; }

        [JsonProperty("Matches_Completed")]
        public int MatchesCompleted { get; set; }

        [JsonProperty("Melee_kills")]
        public int MeleeKills { get; set; }

        [JsonProperty("Midairs")]
        public int Midairs { get; set; }

        [JsonProperty("Multikills")]
        public int Multikills { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Ski_Distance")]
        public int SkiDistance { get; set; }

        [JsonProperty("Sprees")]
        public int Sprees { get; set; }

        [JsonProperty("Tag")]
        public string Tag { get; set; }

        [JsonProperty("Top_Speed")]
        public int TopSpeed { get; set; }

        [JsonProperty("Vehicle_Roadkills")]
        public int VehicleRoadkills { get; set; }

        [JsonProperty("Vehicles_Destroyed")]
        public int VehiclesDestroyed { get; set; }

        [JsonProperty("ret_msg")]
        public object RetMsg { get; set; }
    }
}