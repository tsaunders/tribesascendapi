﻿using Newtonsoft.Json;

namespace TribesAscendAPI.Data
{
    public class Session
    {
        [JsonProperty("ret_msg")]
        public string RetMsg { get; set; }

        [JsonProperty("session_id")]
        public string SessionId { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
    }
}