﻿using Newtonsoft.Json;

namespace TribesAscendAPI.Data
{
    public class DataUsed
    {
        [JsonProperty("Active_Sessions")]
        public int ActiveSessions { get; set; }

        [JsonProperty("Concurrent_Sessions")]
        public int ConcurrentSessions { get; set; }

        [JsonProperty("Request_Limit_Daily")]
        public int RequestLimitDaily { get; set; }

        [JsonProperty("Session_Cap")]
        public int SessionCap { get; set; }

        [JsonProperty("Session_Time_Limit")]
        public int SessionTimeLimit { get; set; }

        [JsonProperty("Total_Requests_Today")]
        public int TotalRequestsToday { get; set; }

        [JsonProperty("Total_Sessions_Today")]
        public int TotalSessionsToday { get; set; }

        [JsonProperty("ret_msg")]
        public object RetMsg { get; set; }
    }
}
