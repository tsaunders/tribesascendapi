﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TribesAscendAPI.Data
{
    public enum Maps
    {
        None,
        [Description("Air Arena")]
        AirArena,
        [Description("Arx Novena")]
        ArxNovena,
        [Description("Bella Omega")]
        BellaOmega,
        Blueshift,
        [Description("Canyon Crusade Revival")]
        CanyonCrusadeRevival,
        Crossfire,
        [Description("Dangerous Crossing")]
        DangerousCrossing,
        Drydock,
        [Description("Drydock Night")]
        DrydockNight,
        Fraytown,
        Hinterlands,
        Inferno,
        Katabatic,
        [Description("Lava Arena")]
        LavaArena,
        Miasma,
        Nightabatic,
        Outskirts,
        Permafrost,
        Quicksand,
        Raindance,
        Stonehenge,
        [Description("Sulfur Cove")]
        SulfurCove,
        Sunstar,
        Tartarus,
        [Description("Temple Ruins")]
        TempleRuins,
        Undercroft,
        [Description("Walled In")]
        WalledIn,
        Whiteout
    };
}
