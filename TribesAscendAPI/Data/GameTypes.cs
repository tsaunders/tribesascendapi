﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TribesAscendAPI.Data
{
    public enum GameTypes
    {
        None,
        Arena,
        CaH,
        CTF,
        CTFBlitz,
        Rabbit,
        TDM
    };
}
