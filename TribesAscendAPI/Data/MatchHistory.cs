﻿using Newtonsoft.Json;

namespace TribesAscendAPI.Data
{
    /// <summary>
    /// Will be array of all matches
    /// </summary>
    public class MatchHistory
    {
        [JsonProperty("Assists")]
        public int Assists { get; set; }

        [JsonProperty("Deaths")]
        public int Deaths { get; set; }

        [JsonProperty("Entry_Datetime")]
        public string EntryDatetime { get; set; }

        [JsonProperty("Kills")]
        public int Kills { get; set; }

        [JsonProperty("Map_Name")]
        public string MapName { get; set; }

        [JsonProperty("MatchId")]
        public int MatchId { get; set; }

        [JsonProperty("PlayedBrute")]
        public string PlayedBrute { get; set; }

        [JsonProperty("PlayedDoombringer")]
        public string PlayedDoombringer { get; set; }

        [JsonProperty("PlayedInfiltrator")]
        public string PlayedInfiltrator { get; set; }

        [JsonProperty("PlayedJuggernaught")]
        public string PlayedJuggernaught { get; set; }

        [JsonProperty("PlayedPathfinder")]
        public string PlayedPathfinder { get; set; }

        [JsonProperty("PlayedRaider")]
        public string PlayedRaider { get; set; }

        [JsonProperty("PlayedSentinel")]
        public string PlayedSentinel { get; set; }

        [JsonProperty("PlayedSoldier")]
        public string PlayedSoldier { get; set; }

        [JsonProperty("PlayedTechnician")]
        public string PlayedTechnician { get; set; }

        [JsonProperty("Score")]
        public int Score { get; set; }

        [JsonProperty("Time_In_Match")]
        public int TimeInMatch { get; set; }

        [JsonProperty("ret_msg")]
        public object RetMsg { get; set; }
    }
}
