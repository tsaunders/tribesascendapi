﻿using Newtonsoft.Json;

namespace TribesAscendAPI.Data
{
    /// <summary>
    /// I belive the class and map names will be an array
    /// </summary>
    /// 

    public class TimePlayed
    {
        [JsonProperty("Class")]
        public string Class { get; set; }

        [JsonProperty("Map_Name")]
        public string MapName { get; set; }

        [JsonProperty("Time_Played")]
        public int Time_Played { get; set; }

        [JsonProperty("ret_msg")]
        public object RetMsg { get; set; }
    }
}
