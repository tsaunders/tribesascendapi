﻿
namespace TribesAscendAPI
{
    public class AuthenticationData
    {
        public string developerID { get; set; }
        public string authenticationKey { get; set; }
        public string baseURI { get; set; }
        public string currentSignature { get; set; }
        public string sessionID { get; set; }
        public string currentTimestamp { get; set; }        
    }
}
