﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using TribesAscendAPI;
using TribesAscendAPI.Data;

namespace TribesAscend
{
    //TODO:  Need to figure out if some of the methods that only return one thing are really suppose to be an array.  Waiting to hear back from the developers of the API
    public class TribesAscendAPI
    {
        AuthenticationData authenticationData = new AuthenticationData();        

        //Create base class for return message.  If return message = invalid then tell user they need to recreate connection
        public TribesAscendAPI(string developerID, string authenticationKey, string baseURI)
        {
            authenticationData.developerID = developerID;
            authenticationData.authenticationKey = authenticationKey;
            authenticationData.baseURI = baseURI;
            authenticationData.sessionID = CreateSession();
        }

        private string BuildURL(string method, string value = "")
        {
            string url = "";

            authenticationData.currentTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            authenticationData.currentSignature = GetMD5Hash(authenticationData.developerID + method.Substring(0, method.Length - 4) + authenticationData.authenticationKey + authenticationData.currentTimestamp);

            url = authenticationData.baseURI +
                   method + "/" +
                   authenticationData.developerID + "/" +
                   authenticationData.currentSignature + "/" +
                   authenticationData.sessionID + "/" +
                   authenticationData.currentTimestamp;

            if (!string.IsNullOrEmpty(value))
                url += "/" + value;

            return url;
        }

#region "Core Methods"
        private string CreateSession()
        {
            //Can't use BuildURL since this gets the Session ID;

            authenticationData.currentTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            authenticationData.currentSignature = GetMD5Hash(authenticationData.developerID + "createsession" + authenticationData.authenticationKey + authenticationData.currentTimestamp);

            //Fixed URL call
            string url = authenticationData.baseURI +
                         "createsessionJson/" +
                         authenticationData.developerID + "/" +
                         authenticationData.currentSignature + "/" +
                         authenticationData.currentTimestamp;

            return DownloadSerializedJSON<Session>(url).SessionId;
        }

        //can methods be genericed?
        public List<Player> GetPlayer(string playerName)
        {
            return DownloadSerialziedJSONArray<Player>(BuildURL("getplayerjson", playerName));
        }

        public List<MatchHistory> GetMatchHistory(string playerName)
        {
            return DownloadSerialziedJSONArray<MatchHistory>(BuildURL("getmatchhistoryjson", playerName));
        }

        public List<TimePlayed> GetTimePlayed(string playerName)
        {
            return DownloadSerialziedJSONArray<TimePlayed>(BuildURL("gettimeplayedjson", playerName));
        }

        public List<MatchStats> GetMatchStats(string matchID)
        {
            return DownloadSerialziedJSONArray<MatchStats>(BuildURL("getmatchstatsjson", matchID));
        }
               
        public List<DataUsed> GetDataUsed()
        {
            return DownloadSerialziedJSONArray<DataUsed>(BuildURL("getdatausedJson"));
        }

        /// <summary>
        /// Doesn't return JSON just a string.
        /// </summary>
        /// <returns></returns>
        private Ping Ping()
        {
            string url = authenticationData.baseURI + "pingjson";

            return DownloadSerializedJSON<Ping>(url);
        }
#endregion

#region "Extended Methods"
        /// <summary>
        /// Gets the total time played for a player based on the player / player class / map name
        /// </summary>
        /// <param name="playerName"></param>
        /// <param name="playerClass"></param>
        /// <param name="mapName"></param>
        /// <returns></returns>
        //public int TotalTimePlayed(string playerName, string playerClass = "", string mapName = "")
        public int TotalTimePlayed(string playerName, Classes classes, GameTypes gameType, Maps map)
        {
            int totalTimePlayed = 0;
            
            List<TimePlayed> tp = GetTimePlayed(playerName);

            if(!classes.Equals(Classes.None))
                tp = tp.Where(c => c.Class == classes.Description()).ToList();

            if (!gameType.Equals(GameTypes.None) && !map.Equals(Maps.None))
                tp = tp.Where(m => m.MapName == BuildMapName(gameType, map)).ToList();
            
            //if (!gameType.Equals(GameTypes.None))
            //    tp = tp.Where(m => m.MapName.Contains(gameType.Description())).ToList();

            //if (!map.Equals(Maps.None))
            //    tp = tp.Where(m => m.MapName.Contains(map.Description())).ToList();

            //if (tp.Count = 0)
            //    return totalTimePlayed;

            foreach (TimePlayed t in tp)
            {
                totalTimePlayed += t.Time_Played;
            }

            return totalTimePlayed;
        }

        public int DamageDealt(string playerName, Classes classes, GameTypes gameType, Maps map)
        {
            int totalDamageDealt = 0;

            List<MatchHistory> mh = GetMatchHistory(playerName);
            
            foreach(MatchHistory matchHistory in mh)
            {
                List<MatchStats> ms = GetMatchStats(matchHistory.MatchId.ToString()).Where(p => p.Name == playerName).ToList();
                
                if (!classes.Equals(Classes.None))                    
                    ms = ms.Where(c => c.Class == classes.Description()).ToList();

                if (!gameType.Equals(GameTypes.None) && !map.Equals(Maps.None))
                    //tp = tp.Where(m => m.MapName == BuildMapName(gameType, map)).ToList();
                    ms = ms.Where(m => m.MapName == BuildMapName(gameType, map)).ToList();                

                foreach(MatchStats matchStats in ms)
                {
                    totalDamageDealt += matchStats.Damage;
                }
            }

            return totalDamageDealt;
        }

        public int TotalMatchDamage(string matchID)
        {
            int totalMatchDamage = 0;

            List<MatchStats> ms = GetMatchStats(matchID);

            foreach(MatchStats matchStats in ms)
            {
                totalMatchDamage += matchStats.Damage;
            }

            return totalMatchDamage;
        }

        private string BuildMapName(GameTypes gt, Maps m)
        {
            return gt + " - " + m.Description();            
        }

#endregion

        private List<T> DownloadSerialziedJSONArray<T>(string url) where T : new()
        {
            using (var webClient = new WebClient())
            {
                var json_data = string.Empty;

                try
                {
                    json_data = webClient.DownloadString(url);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException);
                }

                return JsonConvert.DeserializeObject<List<T>>(json_data);             
            }
        }

        private T DownloadSerializedJSON<T>(string url) where T : new()
        {
            using (var webClient = new WebClient())
            {
                var json_data = string.Empty;

                try
                {
                    json_data = webClient.DownloadString(url);
                }
                catch (Exception)
                {
                    //Place excetpion handeling here
                }

                //Will need to remove if they fix the api to not pass a json array unless necessary.
                if (json_data.Substring(0, 1) == "[")
                    json_data = json_data.Substring(1, json_data.Length - 2);

                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }

        /// <summary>
        /// Create Signature Hash
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string GetMD5Hash(string input)
        {
            var md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var bytes = System.Text.Encoding.UTF8.GetBytes(input);
            bytes = md5.ComputeHash(bytes);
            var sb = new System.Text.StringBuilder();
            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("x2").ToLower());
            }
            return sb.ToString();
        }
    }
}
